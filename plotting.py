import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    # prior_decay = ['prior_decay=0_003', 'prior_decay=0_001', 'prior_decay=0_0005', 'prior_decay=1e-05']
    # variance = ['variance=0_01', 'variance=0_04', 'variance=0_09', 'variance=0_25', 'variance=0_36']
    # demo_type = ['demo_type=uav_2', 'demo_type=uav_wrong']
    # path = r'd:\philly_logs\a3c-off-policy-old'

    environment = ['uav-v0']
    traj_limt = ['tra_lim=1', 'tra_lim=100', 'tra_lim=20', 'tra_lim=500']
    demo_type = ['exp_pat=uav-', 'exp_pat=uav_wrong']
    path = r'c:\d\logs_ddpgfd-master'

    # plot_type = 'error'
    plot_type = 'success'

    if plot_type == 'success':
        for e in environment:
            for d in demo_type:
                log_set = []
                for file in os.listdir(path):
                    # print("origin file", file, d, e)
                    if d in file and e in file and 'see=2' in file:
                        label = ''
                        print("file", file)
                        for v in traj_limt:
                            if v in file:
                                label = v
                        log_set.append([os.path.join(path, file, 'log.txt'), label])

                print(log_set)
                # exit(0)
                title = e + '_' + d + '_' + 'success_rate'
                plt.figure(title)
                for f in range(len(log_set)):
                    print("current f", log_set[f][0])
                    data = open(log_set[f][0])
                    label = log_set[f][1]
                    line = data.readline()

                    training_steps, returns = [], []
                    while line:
                        if 'return-average' in line and 'd-return-average' not in line:
                            try:
                                reward = float(line.split('|')[2])
                                returns.append(reward)
                            except:
                                pass
                        if 'total-samples' in line:
                            try:
                                step = int(line.split('|')[2])
                                training_steps.append(step)
                            except:
                                pass
                        line = data.readline()

                    L_t = len(training_steps)
                    L_r = len(returns)
                    print("length", L_t, L_r)
                    if L_t < L_r:
                        returns = returns[0:L_t]
                    if L_t > L_r:
                        training_steps = training_steps[0:L_r]

                    plt.plot(training_steps, returns, label=label, linewidth=3.0)
                    plt.ylim([0,1.1])
                    plt.xlabel('training steps')
                    plt.ylabel('success rates')
                plt.legend()
                plt.grid()
                title = title.replace('.', '_')
                plt.title(title)
                # print(title)
                title += '_ddpgfd'
                plt.savefig(title)
                plt.show()
                plt.close()
                # exit(0)

    elif plot_type == 'error':
        for e in environment:
            for d in demo_type:
                for p in prior_decay:
                    # for v in variance:
                    log_set = []
                    for file in os.listdir(path):
                        print("origin file", file, d, p, e)
                        if d in file and p in file and e in file and 'rew_typ' not in file:
                            label = ''
                            # print("file", file)
                            for v in variance:
                                if v in file:
                                    label = v
                            log_set.append([os.path.join(path, file, 'TD_Error.txt'), label])

                    # print(log_set)
                    # exit(0)
                    title = e + '_' + d + '_' + p + '_' + 'td_errors'
                    plt.figure(title)
                    for f in range(len(log_set)):
                        data = open(log_set[f][0])
                        label = log_set[f][1]
                        line = data.readline()

                        training_steps = []
                        td_errors = []
                        while line:
                            try:
                                ste = str(int(line.split(' ')[1]))
                                err = float(line.split(' ')[0])
                                training_steps.append(ste)
                                td_errors.append(err)
                            except:
                                print(f, line)
                                pass

                            line = data.readline()

                        data = dict(zip(training_steps, td_errors))
                        training_steps = list(data.keys())
                        td_errors = list(data.values())
                        training_steps = training_steps[::100]
                        td_errors = td_errors[::100]
                        training_steps = [int(step) for step in training_steps]
                        td_errors = [np.log(error) for error in td_errors]

                        plt.plot(training_steps, td_errors, label=label, linewidth=3.0)
                        plt.xlabel('training steps')
                        plt.ylabel('TD errors')
                    plt.legend()
                    plt.grid()
                    plt.title(title)
                    plt.savefig(title)
                    plt.show()
                    plt.close()
                    # exit(0)
    else:
        pass