import os
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    # prior_decay = ['prior_decay=0_003', 'prior_decay=0_001', 'prior_decay=0_0005', 'prior_decay=1e-05']
    # variance = ['variance=0_01', 'variance=0_04', 'variance=0_09', 'variance=0_25', 'variance=0_36']
    # demo_type = ['demo_type=uav_2', 'demo_type=uav_wrong']
    # path = r'd:\philly_logs\a3c-off-policy-old'

    environment = ['uav-v0']
    # reward_coef = ['rew_coe=0.01', 'rew_coe=0.005', 'rew_coe=0.05', 'rew_coe=0.1']
    reward_coef = ['rew_coe=0.01', 'rew_coe=0.1']
    # traj_limt = ['tra_lim=1', 'tra_lim=100', 'tra_lim=20', 'tra_lim=500']
    traj_limt = ['tra_lim=100', 'tra_lim=500']
    demo_type = ['exp_pat=uav_wrong']
    # demo_type = ['exp_pat=uav-']
    path = r'c:\d\logs_pofd'
    seed = ['1', '2', '3']
    # variance = ['var=0.0', 'var=0.4']
    variance = ['var=0.0']

    # plot_type = 'error'
    plot_type = 'success'

    if plot_type == 'success':
        for e in environment:
            for d in demo_type:
                for p in reward_coef:
                    for t in traj_limt:
                        log_set = []
                        for v in variance:
                            log_set_sub = []
                            for file in os.listdir(path):
                                if d in file and p in file and e in file and t in file and v in file:
                                    label = ''
                                    for s in seed:
                                        if s in file:
                                            label = s
                                    log_set_sub.append([os.path.join(path, file, 'log.txt'), label])
                            log_set.append([log_set_sub, v])

                        print("available logs", log_set)
                        # exit(0)
                        title = e + '_' + d + '_' + p + '_' + 'success_rate'
                        figsize = 8, 7
                        plt.figure(figsize=figsize)
                        plt.rcParams.update({'font.size': 20, 'font.family': 'serif', 'font.serif': 'Times New Roman'})
                        plt.gca().yaxis.get_major_formatter().set_powerlimits((0, 1))
                        plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 1))

                        for f in range(len(log_set)):
                            data_first = log_set[f][0]
                            label_first = log_set[f][1]
                            print("data first and label first", data_first, label_first)
                            success_rates_total = []
                            training_steps_total = []
                            for g in range(len(data_first)):
                                data_second = open(data_first[g][0])
                                label_second = data_first[g][1]
                                line = data_second.readline()
                                training_steps = []
                                success_rates = []
                                episode_return = []
                                while line:
                                    if 'return-average' in line and 'd-return-average' not in line:
                                        try:
                                            reward = float(line.split('|')[2])
                                            success_rates.append(reward)
                                        except:
                                            print(line)
                                            pass
                                    if 'total-samples' in line:
                                        try:
                                            step = int(line.split('|')[2])
                                            training_steps.append(step)
                                        except:
                                            print(line)
                                            pass
                                    line = data_second.readline()

                                print("consistrence check", len(success_rates), len(training_steps))
                                success_rates_total.append(success_rates)
                                training_steps_total.append(training_steps)

                            selected_success_rates = []
                            selected_training_steps = []
                            for i in range(len(success_rates_total)):
                                if len(success_rates_total[i]) <= 200:
                                    pass
                                else:
                                    selected_success_rates.append(success_rates_total[i][0:200])
                                    selected_training_steps.append(training_steps_total[i][0:200])
                            if selected_success_rates:
                                print("total selected seeds", len(selected_success_rates))
                                success_rates = np.array(selected_success_rates)
                                success_rates = np.mean(success_rates, 0)
                                training_steps = selected_training_steps[0]
                                plt.plot(training_steps, success_rates, linewidth=3.0)

                        print("shape check", np.shape(training_steps), np.shape(success_rates), type(training_steps),
                              type(success_rates))
                        if len(training_steps) < len(success_rates):
                            success_rates = success_rates[0:len(training_steps)]
                        if len(training_steps) > len(success_rates):
                            training_steps = training_steps[0:len(success_rates)]
                        print(training_steps, success_rates)
                        data = np.concatenate(
                            (np.expand_dims(np.array(training_steps), 0), np.expand_dims(success_rates, 0)), 0)
                        # name = 'POfD_' + "uav" + '.npy'
                        name = 'POfD_' + "uav_wrong" + '.npy'
                        np.save(name, data)

                        plt.plot(training_steps, success_rates, linewidth=3.0)

                        plt.xlabel('training step')
                        plt.ylabel('success rate')

                        plt.legend()
                        plt.grid()
                        plt.title(title)
                        # plt.savefig(title)
                        plt.show()
                        plt.close()



    # elif plot_type == 'error':
    #     for e in environment:
    #         for d in demo_type:
    #             for p in prior_decay:
    #                 # for v in variance:
    #                 log_set = []
    #                 for file in os.listdir(path):
    #                     print("origin file", file, d, p, e)
    #                     if d in file and p in file and e in file and 'rew_typ' not in file:
    #                         label = ''
    #                         # print("file", file)
    #                         for v in variance:
    #                             if v in file:
    #                                 label = v
    #                         log_set.append([os.path.join(path, file, 'TD_Error.txt'), label])
    #
    #                 # print(log_set)
    #                 # exit(0)
    #                 title = e + '_' + d + '_' + p + '_' + 'td_errors'
    #                 plt.figure(title)
    #                 for f in range(len(log_set)):
    #                     data = open(log_set[f][0])
    #                     label = log_set[f][1]
    #                     line = data.readline()
    #
    #                     training_steps = []
    #                     td_errors = []
    #                     while line:
    #                         try:
    #                             ste = str(int(line.split(' ')[1]))
    #                             err = float(line.split(' ')[0])
    #                             training_steps.append(ste)
    #                             td_errors.append(err)
    #                         except:
    #                             print(f, line)
    #                             pass
    #
    #                         line = data.readline()
    #
    #                     data = dict(zip(training_steps, td_errors))
    #                     training_steps = list(data.keys())
    #                     td_errors = list(data.values())
    #                     training_steps = training_steps[::100]
    #                     td_errors = td_errors[::100]
    #                     training_steps = [int(step) for step in training_steps]
    #                     td_errors = [np.log(error) for error in td_errors]
    #
    #                     plt.plot(training_steps, td_errors, label=label, linewidth=3.0)
    #                     plt.xlabel('training steps')
    #                     plt.ylabel('TD errors')
    #                 plt.legend()
    #                 plt.grid()
    #                 plt.title(title)
    #                 plt.savefig(title)
    #                 plt.show()
    #                 plt.close()
    #                 # exit(0)
    # else:
    #     pass